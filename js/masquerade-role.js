(function ($, Drupal, drupalSettings) {
  if (window.top === window.self) {
    $('body').append(drupalSettings.msqrole.markup);
  }
})(jQuery, Drupal, drupalSettings);
