<?php

namespace Drupal\msqrole\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\msqrole\RoleManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MasqueradeRoleReset
 *
 * @package Drupal\msqrole\Controller
 */
class MasqueradeRoleController extends ControllerBase {

  /**
   * The role manager.
   *
   * @var \Drupal\msqrole\RoleManagerInterface
   */
  protected RoleManagerInterface $roleManager;

  /**
   * The redirect destination object.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected RedirectDestinationInterface $destination;

  /**
   * MasqueradeRoleReset constructor.
   *
   * @param \Drupal\msqrole\RoleManagerInterface $role_manager
   *   The role manager.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $destination
   *   The redirect destination object.
   */
  public function __construct(
    RoleManagerInterface $role_manager,
    RedirectDestinationInterface $destination,
  ) {
    $this->roleManager = $role_manager;
    $this->destination = $destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('msqrole.manager'),
      $container->get('redirect.destination'),
    );
  }

  /**
   * This is an "API" call to set the roles via direct link.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response object.
   */
  public function set(Request $request) {
    $key = $request->query->get('key');
    if (!$roles = $this->roleManager->getRolesForKey($key)) {
      $this->messenger()->addWarning('Invalid Masquerade Role key was given.');
      return $this->redirect('<front>');
    }

    $this->roleManager->setActive($this->currentUser()->id(), TRUE);
    $this->roleManager->setRoles($this->currentUser()->id(), $roles);

    // Invalidate cache tags.
    try {
      $this->roleManager->invalidateTags($this->currentUser()->id());
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      // Do nothing, just redirect to the front page.
    }

    return $this->doRedirect();
  }

  /**
   * Resets msqrole data and redirects back to front.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response object.
   */
  public function reset() {
    $this->roleManager->removeData($this->currentUser()->id());

    // Invalidate cache tags.
    try {
      $this->roleManager->invalidateTags($this->currentUser()->id());
    } catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      // Do nothing if this fails.
    }

    return $this->doRedirect();
  }

  /**
   * Checks whether the user has access, and msqrole is not active.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Request $request = NULL) {
    $request = $request ?? \Drupal::request();
    if (!$key = $request->query->get('key')) {
      return AccessResult::forbidden('Either no key or an invalid key was given.');
    }

    return $this->roleManager->checkAccessForKey($key, $account);
  }

  /**
   * Checks whether msqrole is active.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function accessReset(AccountInterface $account) {
    return AccessResult::allowedIf($this->roleManager->isActive($account->id()));
  }

  /**
   * Provides a redirect to the correct endpoint.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  protected function doRedirect(): RedirectResponse {
    try {
      $destination = Url::fromUserInput($this->destination->get());
      if ($destination->isRouted() && $destination->access() && $destination->getRouteName() !== 'msqrole.reset') {
        return $this->redirect($destination->getRouteName(), $destination->getRouteParameters());
      }
    } catch(\Exception $e) {
      // Do nothing, just redirect to the front page.
    }

    return $this->redirect('<front>');
  }

}
