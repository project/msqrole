<?php

namespace Drupal\msqrole;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user\UserDataInterface;

/**
 * Class RoleManager.
 *
 * @package Drupal\msqrole
 */
class RoleManager implements RoleManagerInterface {

  /**
   * The user data instance.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected UserDataInterface $userData;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The key value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected KeyValueFactoryInterface $keyValueFactory;

  /**
   * The uuid generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuid;

  /**
   * Constructs the RoleManager class.
   *
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value factory.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The uuid generator.
   */
  public function __construct(
    UserDataInterface $user_data,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    KeyValueFactoryInterface $key_value_factory,
    UuidInterface $uuid,
  ) {
    $this->userData = $user_data;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->keyValueFactory = $key_value_factory;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritDoc}
   */
  public function isPermissionInRoles($permission, array $roles) {
    $roles = $this->getAllRoles($roles);
    if (!$roles) {
      return FALSE;
    }

    foreach ($roles as $role) {
      if ($role->hasPermission($permission)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getAllRoles(?array $role_ids = NULL) {
    if (is_array($role_ids)) {
      return $this->entityTypeManager
        ->getStorage('user_role')
        ->loadMultiple($role_ids);
    }

    return $this->entityTypeManager
      ->getStorage('user_role')
      ->loadMultiple();
  }

  /**
   * {@inheritDoc}
   */
  public function getConfigurableRoles() {
    $roles = $this->getAllRoles();
    $disallow_roles = [
      'anonymous',
      'authenticated',
      'administrator',
    ];

    // Unset roles that shouldn't be masqueraded as.
    foreach ($disallow_roles as $role) {
      if (!isset($roles[$role])) {
        continue;
      }
      unset($roles[$role]);
    }

    return $roles;
  }

  /**
   * {@inheritDoc}
   */
  public function getRoles($uid) {
    $data = [];
    if (!empty($this->getData($uid, 'roles'))) {
      $data = unserialize($this->getData($uid, 'roles'));
    }
    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getData($uid, string $key) {
    return $this->userData->get('msqrole', $uid, $key);
  }

  /**
   * {@inheritDoc}
   */
  public function setRoles($uid, array $roles) {
    return $this->setData($uid, 'roles', serialize($roles));
  }

  /**
   * {@inheritDoc}
   */
  public function setData($uid, string $key, $value) {
    return $this->userData->set('msqrole', $uid, $key, $value);
  }

  /**
   * {@inheritDoc}
   */
  public function isActive($uid) {
    return $this->getData($uid, 'active') ?? FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function setActive($uid, bool $active) {
    return $this->setData($uid, 'active', $active ? 1 : 0);
  }

  /**
   * {@inheritDoc}
   */
  public function removeData($uid, ?string $key = NULL) {
    return $this->userData->delete('msqrole', $uid, $key);
  }

  /**
   * {@inheritDoc}
   */
  public function invalidateTags($uid) {
    $config = $this->configFactory->get('msqrole.settings');
    $theme_config = $this->configFactory->get('system.theme');
    $admin_theme = $theme_config->get('admin');
    $default_theme = $theme_config->get('default');

    // Replace possible variables in the tags.
    $tags = Cache::mergeTags(
      RoleManagerInterface::TAGS_TO_INVALIDATE,
      (unserialize($config->get('tags_to_invalidate')) ?: [])
    );
    foreach ($tags as &$tag) {
      $tag = str_replace('[user:uid]', $uid, $tag);
      $tag = str_replace('[theme:admin]', $admin_theme, $tag);
      $tag = str_replace('[theme:default]', $default_theme, $tag);
    }
    $tags = array_filter($tags);

    Cache::invalidateTags($tags);
  }

  /**
   * {@inheritDoc}
   */
  public function generateUrl(array $roles, bool $single_usage = FALSE, array $url_options = []): Url {
    // Remove roles that don't exist.
    $role_storage = $this->entityTypeManager->getStorage('user_role');
    foreach ($roles as $key => $role_id) {
      if (!$role_storage->load($role_id)) {
        unset($roles[$key]);
      }
    }

    // If no roles are left in the array, we need to throw an exception.
    if (empty($roles)) {
      throw new \BadMethodCallException('You need to provide at least one existing role.');
    }

    // If not single usage, we should make the key reusable, so we don't end up
    // with too many orphaned entries.
    $key = $single_usage ? $this->uuid->generate() : implode(',', $roles);
    $hash = hash('sha256', $key);
    $key_value = $this->keyValueFactory->get('msqrole.urls');
    $key_value->setIfNotExists($hash, [
      'persist' => !$single_usage,
      'roles' => $roles,
      'created' => time(),
    ]);

    $url_options['query']['key'] = $hash;
    return Url::fromRoute('msqrole.set', [], $url_options);
  }

  /**
   * {@inheritDoc}
   */
  public function getRolesForKey(string $key, bool $delete_single_usage_keys = TRUE) {
    $key_value = $this->keyValueFactory->get('msqrole.urls');
    if (!$entry = $key_value->get($key)) {
      return FALSE;
    }

    $roles = $entry['roles'] ?: [];
    $roles = array_unique(array_merge(['authenticated' => 'authenticated'], $roles));

    // Destroy the entry if it was meant to be used only once.
    if ($delete_single_usage_keys && empty($entry['persist'])) {
      $key_value->delete($key);
    }

    return $roles;
  }

  /**
   * {@inheritDoc}
   */
  public function checkAccessForKey(string $key, AccountInterface $account, bool $return_as_object = TRUE) {
    if (!$roles = $this->getRolesForKey($key, FALSE)) {
      return $return_as_object ? AccessResult::forbidden('The given key is invalid.') : FALSE;
    }

    $roles_result = !$this->isActive($account->id());
    foreach ($roles as $role) {
      if ($role === 'authenticated') {
        continue;
      }

      $roles_result = $roles_result && $account->hasPermission('masquerade as ' . $role);
      if (!$roles_result) {
        break;
      }
    }

    return $return_as_object ? AccessResult::allowedIf($roles_result) : $roles_result;
  }

}
