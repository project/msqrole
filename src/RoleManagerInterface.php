<?php

namespace Drupal\msqrole;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;

/**
 * Interface RoleManagerInterface.
 *
 * @package Drupal\msqrole
 */
interface RoleManagerInterface {

  /**
   * Tags to invalidate when masquerading as different role.
   */
  const TAGS_TO_INVALIDATE = [
    'config:block.block.[theme:admin]_local_actions',
    'config:block.block.[theme:admin]_local_tasks',
    'config:block.block.[theme:default]_local_actions',
    'config:block.block.[theme:default]_local_tasks',
    'config:system.menu.account',
    'config:system.menu.admin',
    'config:system.menu.tools',
    'local_task',
    'local_action',
    'user:[user:uid]',
  ];

  /**
   * Checks whether permission is in given roles.
   *
   * @param $permission
   *   The permission to be checked.
   * @param array $roles
   *   The roles to search the permission in.
   *
   * @return bool
   *   Whether given roles contain permission.
   */
  public function isPermissionInRoles($permission, array $roles);

  /**
   * Returns all existing roles or roles based on given ids.
   *
   * @param array|null $role_ids
   *   Array of role ids to load.
   *
   * @return \Drupal\user\RoleInterface[]
   *   Array of roles.
   */
  public function getAllRoles(?array $role_ids = NULL);

  /**
   * Returns configurable roles.
   *
   * @return \Drupal\user\RoleInterface[]
   *   The configurable roles.
   */
  public function getConfigurableRoles();

  /**
   * Returns the role the current user is masquerading as.
   *
   * @return array
   *   User roles as an array of strings.
   */
  public function getRoles($uid);

  /**
   * Sets roles in user data.
   *
   * @param mixed $uid
   *   The user ID to set roles for.
   * @param array $roles
   *   The roles to set in user data.
   *
   * @return mixed
   */
  public function setRoles($uid, array $roles);

  /**
   * Whether the current user is masquerading.
   *
   * @return bool
   *   Whether masquerade role is active.
   */
  public function isActive($uid);

  /**
   * Sets active state in user data.
   *
   * @param mixed $uid
   *   The user id to set module active for.
   * @param bool  $active
   *   TRUE for active, FALSE for inactive.
   *
   * @return mixed
   */
  public function setActive($uid, bool $active);

  /**
   * Returns user data from config.
   *
   * @param mixed  $uid
   *   The user ID to get user data for.
   * @param string $key
   *   The user data key to return.
   *
   * @return null|array|mixed
   *   The requested data.
   */
  public function getData($uid, string $key);

  /**
   * Sets user data based on provided key/value.
   *
   * @param mixed  $uid
   *   The user ID to set user data for.
   * @param string $key
   *   The user data key to set.
   * @param mixed  $value
   *   The value to set for user data key.
   *
   * @return mixed
   *   The result of setting the data.
   */
  public function setData($uid, string $key, $value);

  /**
   * Removes user data.
   *
   * @param mixed  $uid
   *   The user ID to remove user data from.
   * @param string $key
   *   The user data key to remove.
   *
   * @return mixed
   *   The result of removing the data.
   */
  public function removeData($uid, string $key);

  /**
   * Resets cache for given user id.
   *
   * @param string|int $uid
   *   The user ID to reset cache tags for.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function invalidateTags($uid);

  /**
   * Generates a Masquerade Role url.
   *
   * @param array $roles
   *   The role IDs (not entities) to generate a link for.
   * @param bool $single_usage
   *   Whether the URL should be destroyed after a single use.
   * @param array $url_options
   *   The URL options (optional).
   *
   * @return \Drupal\Core\Url
   *   The generated URL.
   *
   * @throws \BadMethodCallException
   *   When the given roles array is empty or contains only non-existing roles.
   */
  public function generateUrl(array $roles, bool $single_usage = FALSE, array $url_options = []): Url;

  /**
   * Retrieves the roles for a given key.
   *
   * @param string $key
   *   The unique key/hash.
   * @param bool $delete_single_usage_keys
   *   Whether keys marked as single usage should be deleted.
   *
   * @return bool|array
   */
  public function getRolesForKey(string $key, bool $delete_single_usage_keys = TRUE);

  /**
   * Checks access for a given key.
   *
   * @param string $key
   *   The key containing the roles.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check access for.
   * @param bool $return_as_object
   *   Whether the result should be an AccessResultInterface object.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   */
  public function checkAccessForKey(string $key, AccountInterface $account, bool $return_as_object = TRUE);

}
